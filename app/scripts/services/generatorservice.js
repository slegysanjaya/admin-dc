'use strict';

/**
 * @ngdoc service
 * @name adminDcApp.generatorService
 * @description
 * # generatorService
 * Service in the adminDcApp.
 */
angular.module('adminDcApp')
  .service('generatorService', function () {
    this.indexGenerator = function (data){
    	if (data != null || data != undefined){
	    	for (var i=0; i<data.length; i++){
	    		data[i].index = i + 1;
	    	}
	    	return data;
	    }else{
	    	return [];
	    }
    };
  });
