'use strict';

/**
 * @ngdoc service
 * @name adminDcApp.AuthService
 * @description
 * # AuthService
 * Service in the adminDcApp.
 */
angular.module('adminDcApp')
  .service('AuthService', function ($log, $http, $q, URL_SERVER, $rootScope, $localStorage, $state, growl) {
    
    var me = this;

    me.login = function(email, password) {
      var data = {
        'email' : email,
        'password' : password
      };
      return $http.post(URL_SERVER + '/api/auth/login', data)
        .then(function (response) {
          // simpan token ke local storage
          $localStorage.token = response.data.data;
          // update headers $http
          $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;
          
          me.cekRole();
          
        }, function (error) {
          $log.error(error.data.message);
          growl.error(error.data.message);
        });
    };

    me.cekRole = function(){
    	$http.get(URL_SERVER+'/api/auth/roles')
    	.then(function(response){
    		$localStorage.role = response.data.data[0];
	    		if($localStorage.role === 'admin'){
				$state.go('home');
			}else{
				delete $localStorage.token;
				delete $localStorage.role;
				growl.warning('Anda tidak mempunyai role admin');

			}
    	});
       	
		
    };

    
  });
