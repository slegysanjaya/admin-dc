'use strict';

/**
 * @ngdoc service
 * @name adminDcApp.adminService
 * @description
 * # adminService
 * Service in the adminDcApp.
 */
angular.module('adminDcApp')
  .service('adminService', function ($http, URL_SERVER) {
    var me = this;

    me.getDataAdmin = function(){
      	return $http.get(URL_SERVER + '/api/admin');
    };

    me.getTopPlace = function(){
     	return $http.get(URL_SERVER + '/api/admin/topPlace');
    };

    me.analyticUserProvince = function(selectProvince){
     	return $http.get(URL_SERVER + '/api/admin/user/analytics/'+selectProvince);
    };

    me.analyticUser = function(){
      return $http.get(URL_SERVER + '/api/admin/user/analytics');
    };

    me.mostViewed = function(){
     	return $http.get(URL_SERVER + '/api/admin/mostViewed');
    };

     me.mostSearched = function(){
     	return $http.get(URL_SERVER + '/api/admin/mostSearched');
    };
	
  	me.addTopPlace = function(id){
  		var data = {
  			place_id : id
  		};
        	return $http.post(URL_SERVER + '/api/admin/topPlace/add', data);
      };

    me.deleteTopPlace = function(id){
  		var data = {
  			place_id : id
  		};
        	return $http.post(URL_SERVER + '/api/admin/topPlace/delete', data);
      };

    me.getAllPlace = function(){
        	return $http.get(URL_SERVER + '/api/admin/allPlace');
      };

    me.allPlaceNotVerify = function(){
          return $http.get(URL_SERVER + '/api/admin/allPlaceNotVerify');
    };

    me.deletePlace = function(id){
        	return $http.delete(URL_SERVER + '/api/admin/place/'+id);
    };

    me.approvalPlace = function(id){
      var data = {
  			 place_id : id
  	  };
        	return $http.post(URL_SERVER + '/api/admin/approval', data);
    };

});
