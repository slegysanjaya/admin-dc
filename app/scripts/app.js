'use strict';

/**
 * @ngdoc overview
 * @name adminDcApp
 * @description
 * # adminDcApp
 *
 * Main module of the application.
 */
angular
  .module('adminDcApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ngStorage',
    'permission',
    'permission.ui',
    'angular-growl',
    'oc.lazyLoad',
    'angular-loading-bar',
    'cgBusy',
    'bsLoadingOverlay',
    'anim-in-out',
    'angular-jwt',
    'chart.js',
    'angular-table',
    'ui.bootstrap',
    'ngFileUpload'
  ]);

  // constants
angular.module('adminDcApp')
  .constant('URL_SERVER', '');

angular.module('adminDcApp')
  .config(function($stateProvider, $urlRouterProvider, $logProvider) {

    // set untuk mode development
    $logProvider.debugEnabled(true);

    // routing
    $urlRouterProvider.otherwise('/home');
    $stateProvider
      .state('login', {
        url: '/login',
        views: {
          'login': {
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl'
          }
        },
        data:{
          permissions:{
            only: ['GUEST'],
            redirectTo : 'home'
          }
        }
      })
    
      .state('home', {
        url: '/home',
        views: {
          'main': {
            templateUrl: 'views/home.html',
            controller: 'HomeCtrl'
          }
        },
        data:{
          permissions:{
            only: ['ROLE_ADMIN'],
            redirectTo : 'login'
          }
        }
      })
      .state('topPlace', {
        url: '/topPlace',
        views: {
          'main': {
            templateUrl: 'views/topplace.html',
            controller: 'TopplaceCtrl'
          }
        },
        data:{
          permissions:{
            only: ['ROLE_ADMIN'],
            redirectTo : 'login'
          }
        }
      })
      .state('place', {
        url: '/place',
        views: {
          'main': {
            templateUrl: 'views/place.html',
            controller: 'PlaceCtrl'
          }
        },
        data:{
          permissions:{
            only: ['ROLE_ADMIN'],
            redirectTo : 'login'
          }
        }
      })
      .state('placeNotVerify', {
        url: '/placeNotVerify',
        views: {
          'main': {
            templateUrl: 'views/placenotverify.html',
            controller: 'PlacenotverifyCtrl'
          }
        },
        data:{
          permissions:{
            only: ['ROLE_ADMIN'],
            redirectTo : 'login'
          }
        }
      })
      .state('mostViewed', {
        url: '/mostViewed',
        views: {
          'main': {
            templateUrl: 'views/mostviewed.html',
            controller: 'MostviewedCtrl'
          }
        },
        data:{
          permissions:{
            only: ['ROLE_ADMIN'],
            redirectTo : 'login'
          }
        }
      })
      .state('mostSearched', {
        url: '/mostSearched',
        views: {
          'main': {
            templateUrl: 'views/mostsearched.html',
            controller: 'MostsearchedCtrl'
          }
        },
        data:{
          permissions:{
            only: ['ROLE_ADMIN'],
            redirectTo : 'login'
          }
        }
      });
  });

   // angular chart config
  angular.module('adminDcApp')
    .config(function(ChartJsProvider) {
      ChartJsProvider.setOptions({ colors : [
        '#803690',
        '#00ADF9',
        '#DCDCDC',
        '#46BFBD',
        '#FDB45C',
        '#949FB1',
        '#4D5360'
      ]});
    });

  // loading bar config
  angular.module('adminDcApp')
    .config(function(cfpLoadingBarProvider, growlProvider) {
      cfpLoadingBarProvider.latencyThreshold = 5000;
      // cfpLoadingBarProvider.includeSpinner = false;
      growlProvider.globalTimeToLive({success: 5000, error: 5000, warning: 5000, info: 5000});
    });

  // angular-busy config
  angular.module('adminDcApp')
    .value('cgBusyDefaults',{
      message:'Loading...',
      backdrop: true,
      // templateUrl: 'views/login.html',
      delay: 0,
      minDuration: 700
    });

// run
angular.module('adminDcApp')
  .run(function ($log, $rootScope, $state, $http, $localStorage, PermRoleStore, AuthService, cfpLoadingBar) {
    $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;
    // variabel global
    $rootScope.$state = $state;

    // konfigurasi role pengguna
    PermRoleStore.defineRole('GUEST', function(){
      return $localStorage.token === undefined;
    });
    PermRoleStore.defineRole('ROLE_ADMIN', function(){
      return $localStorage.token !== undefined && $localStorage.role === 'admin';
    });

    //> konfigurasi role pengguna

    $rootScope.$on('$viewContentLoading', function() {
        cfpLoadingBar.start();
        $rootScope.busyloading = 1000;
    });

    $rootScope.$on('$viewContentLoaded', function() {
        cfpLoadingBar.complete();
    });
    
    $rootScope.logout = function(){
      delete $localStorage.token;
      delete $localStorage.role;

      $state.go('login');
    };

  });

// when content loaded
angular.element(function () {
  setTimeout(function () {
    $.AdminBSB.browser.activate();
    $.AdminBSB.leftSideBar.activate();
    $.AdminBSB.navbar.activate();
    $.AdminBSB.dropdownMenu.activate();
    $.AdminBSB.input.activate();
    $.AdminBSB.select.activate();

    setTimeout(function () {
      $('.page-loader-wrapper').fadeOut();
    }, 300);
  }, 900);
});


