'use strict';

/**
 * @ngdoc function
 * @name adminDcApp.controller:MostviewedCtrl
 * @description
 * # MostviewedCtrl
 * Controller of the adminDcApp
 */
angular.module('adminDcApp')
  .controller('MostviewedCtrl', function ($log, $state, $rootScope, generatorService, $q, $filter, growl, $localStorage, URL_SERVER, $scope, adminService) {
    $scope.title = 'All Place in Cimahi';

    $scope.dataTampil = [];
    $scope.tampilFilter = [];

     $scope.config = {
      itemsPerPage: 10,
      maxPages: 10,
      fillLastPage: false
    };

    $scope.tampilSearch = function() {
      $scope.tampilFilter = $filter('filter')($scope.dataTampil, $scope.search);
    };

    
    $scope.getMostViewed = function() {
      var deferred = $q.defer();
      $scope.tableLoadingBusy = deferred.promise;
    	adminService.mostViewed()
        .then(function (response) {
          $log.debug('mostViewed');
          $log.debug(response.data);

          $scope.dataTampil = response.data.data;
          $scope.tampilFilter = generatorService.indexGenerator($scope.dataTampil);
          deferred.resolve();
        }, function (error) {
          $log.debug(error);
          growl.error('tidak dapat menampilkan data!!');
          deferred.resolve();
          if(error.status === 401){
          	delete $localStorage.token;
          	delete $localStorage.role;
          	$state.go('login');
          }
        });
    };

    $scope.getMostViewed();
  });
