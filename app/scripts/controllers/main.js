'use strict';

/**
 * @ngdoc function
 * @name adminDcApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminDcApp
 */
angular.module('adminDcApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
