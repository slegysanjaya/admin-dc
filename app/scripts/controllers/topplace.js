'use strict';

/**
 * @ngdoc function
 * @name adminDcApp.controller:TopplaceCtrl
 * @description
 * # TopplaceCtrl
 * Controller of the adminDcApp
 */
angular.module('adminDcApp')
  .controller('TopplaceCtrl', function ($log, $state, $rootScope, generatorService, $q, $filter, growl, $localStorage, URL_SERVER, $scope, adminService) {
    $scope.title = 'Top Place';

    $scope.dataTampil = [];
    $scope.tampilFilter = [];

     $scope.config = {
      itemsPerPage: 10,
      maxPages: 10,
      fillLastPage: false
    };

    $scope.tampilSearch = function() {
      $scope.tampilFilter = $filter('filter')($scope.dataTampil, $scope.search);
    };

    
    $scope.getTopPlace = function() {
      var deferred = $q.defer();
      $scope.tableLoadingBusy = deferred.promise;
    	adminService.getTopPlace()
        .then(function (response) {
          $log.debug('getTopPlace');
          $log.debug(response.data);

          $scope.dataTampil = response.data.data;
          $scope.tampilFilter = generatorService.indexGenerator($scope.dataTampil);
          deferred.resolve();
        }, function (error) {
          $log.debug(error);
          growl.error('Tidak dapat menampilkan top place!!');
          deferred.resolve();
          if(error.status === 401){
          	delete $localStorage.token;
          	delete $localStorage.role;
          	$state.go('login');
          }
        });
    };


    $scope.deleteTopPlace = function(id){
      console.log(id);
      adminService.deleteTopPlace(id)
      .then(function(response){
        $log.debug('delete Place');
           $log.debug(response.data.message);
           growl.success(response.data.message);
           $scope.getTopPlace();
      }, function (error) {
          $log.debug(error.data.message);
          growl.error(error.data.message);
          if(error.status === 401){
            delete $localStorage.token;
            delete $localStorage.role;
            $state.go('login');
          }
        });
    };

    $scope.getTopPlace();
  });
