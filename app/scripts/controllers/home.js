'use strict';

/**
 * @ngdoc function
 * @name adminDcApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the adminDcApp
 */
angular.module('adminDcApp')
  .controller('HomeCtrl', function ($log, $state, $rootScope, $q, $filter, jwtHelper, growl, $localStorage, URL_SERVER, $scope, adminService, $timeout) {
    $rootScope.title = 'DASHBOARD';
    $scope.province = '';
    $scope.selectProvince = '';

    $scope.dataTampil = [];
    $scope.dataTampil2 = [];

   
    // service dashboard
    $scope.getData = function() {
    	adminService.getDataAdmin()
        .then(function (response) {
          $log.debug('getUser');
          $log.debug(response.data);
          $scope.dataAdmin = response.data.data;
        }, function (error) {
          $log.debug(error);
          growl.error('Data User tidak dapat ditampilkan');
          if(error.status === 401){
          	delete $localStorage.token;
          	delete $localStorage.role;
          	$state.go('login');
          }
        });
    };

    $scope.getAnalyticUser = function() {
      var deferred = $q.defer();
      $scope.tableLoadingBusy = deferred.promise;

    	adminService.analyticUser()
        .then(function (response) {
          $log.debug('analyticUser');
          $log.debug(response.data);

          $scope.dataTampil2 = response.data.data;
          $scope.labels = [];
          $scope.data = [[]];

          // untuk grafik
        
          for (var i = 0; i < $scope.dataTampil2.length; i++) {
            $scope.labels[i] = $scope.dataTampil2[i].province;
            $scope.data[0][i] = $scope.dataTampil2[i].jumlah;
          }

          deferred.resolve();
        }, function (error) {
          $log.debug(error);
          growl.error('Data analytic user tidak dapat ditampilkan');
          deferred.resolve();
          if(error.status === 401){
          	delete $localStorage.token;
          	delete $localStorage.role;
          	$state.go('login');
          }
        });
    };

    $scope.getAnalyticUserProvince = function(selectProvince) {
      var deferred = $q.defer();
      $scope.tableLoadingBusy = deferred.promise;

      adminService.analyticUserProvince(selectProvince)
        .then(function (response) {
          $log.debug('analyticUser');
          $log.debug(response.data);

          $scope.dataTampil2 = response.data.data;
          $scope.labels = [];
          $scope.data = [[]];

          // untuk grafik
          for (var i = 0; i < $scope.dataTampil2.length; i++) {
            $scope.labels[i] = $scope.dataTampil2[i].city;
            $scope.data[0][i] = $scope.dataTampil2[i].jumlah;
          }

          deferred.resolve();
        }, function (error) {
          $log.debug(error);
          growl.error('Data analytic user tidak dapat ditampilkan');
          deferred.resolve();
          if(error.status === 401){
            delete $localStorage.token;
            delete $localStorage.role;
            $state.go('login');
          }
        });
    };

    $scope.provinces = function() {
      var deferred = $q.defer();
      $scope.loadingSelecBox = deferred.promise;
      adminService.analyticUser()
        .then(function (response) {
          $scope.provincesss = response.data.data;
          deferred.resolve();
        });
      $scope.getAnalyticUser();
    };


    $scope.provinces();
    $scope.getData();

    $scope.showAnalytic = function(selectProvince){
        if(selectProvince === ''){
          $scope.getAnalyticUser();
        }else{
          $scope.getAnalyticUserProvince(selectProvince);
        }
    };

    $scope.$on('$viewContentLoaded', function(event) {
      $timeout(function() {
        $.AdminBSB.browser.activate();
        $.AdminBSB.leftSideBar.activate();
        $.AdminBSB.navbar.activate();
        $.AdminBSB.dropdownMenu.activate();
        $.AdminBSB.input.activate();
        $.AdminBSB.select.activate();

        $timeout(function () {
          $('.page-loader-wrapper').fadeOut();
        }, 300);
      }, 300);
    });
  
  });
