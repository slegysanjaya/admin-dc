'use strict';

/**
 * @ngdoc function
 * @name adminDcApp.controller:PlaceCtrl
 * @description
 * # PlaceCtrl
 * Controller of the adminDcApp
 */
angular.module('adminDcApp')
  .controller('PlaceCtrl', function ($log, $state, $rootScope, generatorService, Upload, $q, $filter, growl, $localStorage, URL_SERVER, $scope, adminService) {
    $scope.title = 'All Place in Cimahi';

    $scope.dataTampil = [];
    $scope.tampilFilter = [];

    $scope.place = true;
    $scope.addPlace = false;

     $scope.config = {
      itemsPerPage: 10,
      maxPages: 10,
      fillLastPage: false
    };

    $scope.tampilSearch = function() {
      $scope.tampilFilter = $filter('filter')($scope.dataTampil, $scope.search);
    };

    
    $scope.getPlace = function() {
      $scope.place = true;
      $scope.addPlace = false;
      var deferred = $q.defer();
      $scope.tableLoadingBusy = deferred.promise;
    	adminService.getAllPlace()
        .then(function (response) {
          $log.debug('getPlace');
          $log.debug(response.data);

          $scope.dataTampil = response.data.data;
          $scope.tampilFilter = generatorService.indexGenerator($scope.dataTampil);
          deferred.resolve();
        }, function (error) {
          $log.debug(error);
          growl.error('tidak dapat menampilkan data tempat cimahi!!');
          deferred.resolve();
          if(error.status === 401){
          	delete $localStorage.token;
          	delete $localStorage.role;
          	$state.go('login');
          }
        });
    };

    $scope.addTopPlace = function(id){
    	console.log(id);
    	adminService.addTopPlace(id)
    	.then(function(response){
    		$log.debug('add Place');
          $log.debug(response.data);
    		  growl.success(response.data.message);
    	}, function (error) {
          $log.debug(error.data);
          growl.error(error.data.message);
          if(error.status === 401){
          	delete $localStorage.token;
          	delete $localStorage.role;
          	$state.go('login');
          }
      	});
    };

    $scope.deletePlace = function(id){
      console.log(id);
      adminService.deletePlace(id)
      .then(function(response){
        $log.debug('delete Place');
          $log.debug(response.data);
          growl.success(response.data.message);
          $scope.getPlace();
      }, function (error) {
          $log.debug(error.data);
          growl.error(error.data.message);
          if(error.status === 401){
            delete $localStorage.token;
            delete $localStorage.role;
            $state.go('login');
          }
        });
    };

    $scope.getPlace();

    $scope.showAddPlace = function(){
      $scope.place = false;
      $scope.addPlace = true;
    };

    $scope.back = function(){
      $scope.place = true;
      $scope.addPlace = false;
    };

    $scope.submit = function() {
      if ($scope.form.file.$valid && $scope.form.name.$valid && $scope.form.lat.$valid && $scope.form.lng.$valid && $scope.form.address.$valid && $scope.form.description.$valid && $scope.form.place_category_id.$valid  && $scope.file) {
        $scope.upload($scope.name, $scope.lat, $scope.lng, $scope.address, $scope.description, $scope.place_category_id, $scope.file);
      }
    };

    $scope.upload = function (name, lat, lng, address, description, place_category_id, file) {
        var deferred = $q.defer();
        $scope.loadingUpload = deferred.promise;

        Upload.upload({
            url: URL_SERVER +'/api/place',
            data: {
              name: name, 
              lat: lat, 
              lng : lng, 
              address : address, 
              description : description,
              place_category_id: place_category_id,
              photo : file
            }
          })
        .then(function(response){
            $log.debug('add Place');
            $log.debug(response.data.message);
            growl.success(response.data.message);
            deferred.resolve();
            $scope.getPlace();
        }, function (error) {
            $log.debug(error.data.message);
            growl.error('Tambah tempat gagal!!');
            deferred.resolve();
            if(error.status === 401){
              delete $localStorage.token;
              delete $localStorage.role;
              $state.go('login');
            }
          });
    };

  });
