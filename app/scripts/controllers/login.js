'use strict';

/**
 * @ngdoc function
 * @name adminDcApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the adminDcApp
 */
angular.module('adminDcApp')
  .controller('LoginCtrl', function ($log, $scope, AuthService, growl) {
    $log.debug('LoginCtrl');

    $scope.email = null;
    $scope.password = null;

    $scope.login = function () {
        if ($scope.email !== null && $scope.password !== null){
          $scope.loading = AuthService.login($scope.email, $scope.password);
        }else{
          growl.error('Username & Password tidak boleh kosong!!');
        }
    };

  });
