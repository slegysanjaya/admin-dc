'use strict';

/**
 * @ngdoc function
 * @name adminDcApp.controller:PlacenotverifyCtrl
 * @description
 * # PlacenotverifyCtrl
 * Controller of the adminDcApp
 */
angular.module('adminDcApp')
  .controller('PlacenotverifyCtrl', function ($log, $state, $rootScope, generatorService, $q, $filter, growl, $localStorage, URL_SERVER, $scope, adminService) {
    $scope.title = 'All Place in Cimahi';

    $scope.dataTampil = [];
    $scope.tampilFilter = [];

     $scope.config = {
      itemsPerPage: 10,
      maxPages: 10,
      fillLastPage: false
    };

    $scope.tampilSearch = function() {
      $scope.tampilFilter = $filter('filter')($scope.dataTampil, $scope.search);
    };

    
    $scope.getPlace = function() {
      var deferred = $q.defer();
      $scope.tableLoadingBusy = deferred.promise;
    	adminService.allPlaceNotVerify()
        .then(function (response) {
          $log.debug('verifyPlace');
          $log.debug(response.data);

          $scope.dataTampil = response.data.data;
          $scope.tampilFilter = generatorService.indexGenerator($scope.dataTampil);
          deferred.resolve();
        }, function (error) {
          $log.debug(error);
          growl.error('tidak dapat menampilkan data tempat cimahi!!');
          deferred.resolve();
          if(error.status === 401){
          	delete $localStorage.token;
          	delete $localStorage.role;
          	$state.go('login');
          }
        });
    };

    $scope.verifyPlace = function(id){
    	console.log(id);
    	adminService.approvalPlace(id)
    	.then(function(response){
    		$log.debug('verify Place');
          	$log.debug(response.data.message);
    		growl.success(response.data.message);
    		$scope.getPlace();
    	}, function (error) {
          $log.debug(error.data.message);
          growl.error(error.data.message);
          if(error.status === 401){
          	delete $localStorage.token;
          	delete $localStorage.role;
          	$state.go('login');
          }
      	});
    };

    $scope.getPlace();
  });