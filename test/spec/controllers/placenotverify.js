'use strict';

describe('Controller: PlacenotverifyCtrl', function () {

  // load the controller's module
  beforeEach(module('adminDcApp'));

  var PlacenotverifyCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PlacenotverifyCtrl = $controller('PlacenotverifyCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PlacenotverifyCtrl.awesomeThings.length).toBe(3);
  });
});
