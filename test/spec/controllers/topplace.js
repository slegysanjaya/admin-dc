'use strict';

describe('Controller: TopplaceCtrl', function () {

  // load the controller's module
  beforeEach(module('adminDcApp'));

  var TopplaceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TopplaceCtrl = $controller('TopplaceCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TopplaceCtrl.awesomeThings.length).toBe(3);
  });
});
