'use strict';

describe('Controller: MostviewedCtrl', function () {

  // load the controller's module
  beforeEach(module('adminDcApp'));

  var MostviewedCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MostviewedCtrl = $controller('MostviewedCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MostviewedCtrl.awesomeThings.length).toBe(3);
  });
});
