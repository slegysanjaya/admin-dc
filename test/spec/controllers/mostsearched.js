'use strict';

describe('Controller: MostsearchedCtrl', function () {

  // load the controller's module
  beforeEach(module('adminDcApp'));

  var MostsearchedCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MostsearchedCtrl = $controller('MostsearchedCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MostsearchedCtrl.awesomeThings.length).toBe(3);
  });
});
