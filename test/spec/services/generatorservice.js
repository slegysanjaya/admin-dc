'use strict';

describe('Service: generatorService', function () {

  // load the service's module
  beforeEach(module('adminDcApp'));

  // instantiate service
  var generatorService;
  beforeEach(inject(function (_generatorService_) {
    generatorService = _generatorService_;
  }));

  it('should do something', function () {
    expect(!!generatorService).toBe(true);
  });

});
